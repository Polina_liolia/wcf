﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ATMLibrary
{
    [DataContract]
    public class Card
    {
        [DataMember]
        public string CardNumber { get; set; }
        [DataMember]
        public string Code { get; set; }

        public Card(string cardNumber, string code)
        {
            CardNumber = cardNumber;
            Code = code;
        }
        public override string ToString()
        {
            return $"Card: {CardNumber}, Code: {Code}";
        }
    }

    public class CardsComparer : IEqualityComparer<Card>
    {
        public bool Equals(Card x, Card y)
        {
            return (x.CardNumber == y.CardNumber && x.Code == y.Code);
        }

        public int GetHashCode(Card obj)
        {
            return base.GetHashCode();
        }
    }
}
