﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.ServiceModel;

namespace ATMLibrary
{
    [ServiceContract]
    public interface IATMContract
    {
        [OperationContract]
        bool Authorize(Card data);
        [OperationContract]
        decimal BalanceCheck(Card card);
        [OperationContract]
        bool SendMoneyToPhone(ATMRequestData data);
    }
}
