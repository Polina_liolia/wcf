﻿using ATMLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsNTService
{
    public class BankClient
    {
        public Card ClientCard { get; set; }
        public decimal Balance { get; set; }
        public decimal CreditLimit { get; set; }

        public BankClient(Card card, decimal balance, decimal creditLimit)
        {
            ClientCard = card;
            Balance = balance;
            CreditLimit = creditLimit;
        }

        public BankClient(string cardNumber, string code, decimal balance, decimal creditLimit)
        {
            ClientCard = new Card(cardNumber, code);
            Balance = balance;
            CreditLimit = creditLimit;
        }
    }
}
