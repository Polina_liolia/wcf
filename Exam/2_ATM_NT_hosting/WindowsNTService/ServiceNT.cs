﻿using ATMLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace WindowsNTService
{
    [RunInstaller(true)]
    public class ProjectInstaller : Installer
    {
        private ServiceProcessInstaller process;
        private ServiceInstaller service;

        public ProjectInstaller()
        {
            process = new ServiceProcessInstaller();
            process.Account = ServiceAccount.LocalSystem;

            service = new ServiceInstaller();
            service.ServiceName = "lllll ATM ServiceNT lllll";
            service.Description = "My service for ATM";
            service.StartType = ServiceStartMode.Automatic;

            Installers.Add(process);
            Installers.Add(service);
        }
    }

    public partial class ServiceNT : ServiceBase
    {
        // Ссылка на экземпляр ServiceHost.
        ServiceHost service = null;

        public ServiceNT()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            if (service == null)
            {
                // Создание экземпляра ServiceHost.
                service = new ServiceHost(typeof(Service));
                // Начало ожидания прихода сообщений.
                service.Open();
            }
        }

        protected override void OnStop()
        {
            if (service != null)
            {
                // Завершение ожидания прихода сообщений.
                service.Close();
            }
        }
    }


    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)] //one instance for all requests
    public class Service : IATMContract
    {
        private List<BankClient> clients;   //stores clients' cards data, that can be authorized

        public Service()
        {
            //initializing cards data list:
            clients = new List<BankClient>()
            {
               new BankClient(new Card("123456", "111"), 8900, 15000),
               new BankClient(new Card("789012", "222"), (decimal)230.34, 2000),
               new BankClient(new Card("987654", "333"), 12, 0)
            };
        }

        public bool Authorize(Card card)
        {
            bool authorized = false;
            BankClient client = clients.FirstOrDefault(c => new CardsComparer().Equals(c.ClientCard, card) == true);
            if (client != null)
            {
                authorized = true;
            }
            return authorized;
        }

        public decimal BalanceCheck(Card card)
        {
            decimal balance = 0;
            if (Authorize(card))
            {
                BankClient client = clients.FirstOrDefault(c => new CardsComparer().Equals(c.ClientCard, card) == true);
                balance = client.Balance;
            }
            return balance;
        }

        public bool SendMoneyToPhone(ATMRequestData data)
        {
            bool moneySent = false;
            if (Authorize(data.CardData))
            {
                BankClient client = clients.FirstOrDefault(c => new CardsComparer().Equals(c.ClientCard, data.CardData) == true);
                if (data.Sum <= client.Balance)
                {
                    moneySent = true;
                    client.Balance -= data.Sum;
                }
                else if (data.CreditSourse == true && data.Sum <= (client.Balance + client.CreditLimit))
                {
                    moneySent = true;
                    client.CreditLimit -= data.Sum - client.Balance;
                    client.Balance = 0;
                }
            }
            return moneySent;
        }
    }
}
