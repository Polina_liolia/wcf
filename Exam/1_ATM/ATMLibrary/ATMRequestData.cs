﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace ATMLibrary
{
    [DataContract]
    public class ATMRequestData
    {
        [DataMember]
        public Card CardData { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public decimal Sum { get; set; }
        [DataMember]
        public bool CreditSourse { get; set; }

        public ATMRequestData(Card card, string phone, decimal sum, bool creditSourse = false)
        {
            CardData = card;
            Phone = phone;
            CreditSourse = creditSourse;
            Sum = sum;
        }
        public ATMRequestData(string cardNumber, string code, string phone, decimal sum, bool creditSourse = false)
        {
            CardData = new Card(cardNumber, code);
            Phone = phone;
            CreditSourse = creditSourse;
            Sum = sum;
        }

        public override string ToString()
        {
            return $"{CardData.ToString()}, Phone: {Phone}, Credit sourse: {CreditSourse}";
        }
    }
}
