﻿using ATMLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UserATMService
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Uri address = new Uri("http://localhost:4000/IContract");
        BasicHttpBinding binding = new BasicHttpBinding();
        ChannelFactory<IATMContract> factory;
        IATMContract channel;
        Card card;

        public MainWindow()
        {
            InitializeComponent();
        }

        #region Main actions handlers
        private void btnAuth_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (factory == null)
                {
                    CreateChannel();
                }
                if (factory != null && channel != null)
                {
                    bool authorized = Authorize();

                    //displaying client's action:
                    txtResponses.Text += $"Клиент: Авторизация {card.CardNumber}{Environment.NewLine}";
                    string authDisplay = authorized ? "OK" : "Authorization failed";
                    txtResponses.Text += $"Сервер: {authDisplay} {Environment.NewLine}";
                    //clearing all UI text inputs after data had been sent:
                    ClearAllTextBoxes();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        
        private void btnCheckBalance_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (factory == null)
                {
                    CreateChannel();
                }
                if (factory != null && card == null)
                {
                    Authorize();
                }
                if(card != null)
                {
                    txtResponses.Text += $"Клиент: Проверка баланса {card.CardNumber}{Environment.NewLine}";
                    decimal balance = channel.BalanceCheck(card);
                    txtResponses.Text += $"Сервер: баланс ${balance} {Environment.NewLine}";
                    ClearAllTextBoxes();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnSendMoneyToPhone_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (factory == null)
                {
                    CreateChannel();
                }
                if (factory != null && card == null)
                {
                    Authorize();
                }
                if (card != null)
                {
                    decimal sum;
                    bool parsed = Decimal.TryParse(txtPhoneSum.Text, out sum);
                    if (!parsed)
                    {
                        MessageBox.Show("Can not parse sum!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    txtResponses.Text += $"Клиент: пополнение мобильного {txtPhone.Text} на ${sum} с карты {card.CardNumber}{Environment.NewLine}";
                    ATMRequestData data = new ATMRequestData(card, txtPhone.Text, sum, (bool)chBoxIsCredit.IsChecked);
                    bool success = channel.SendMoneyToPhone(data);
                    string outputText = success ? "Пополнение успешно" : "Пополнение не произошло";
                    txtResponses.Text += $"Сервер: {outputText} {Environment.NewLine}";
                    ClearAllTextBoxes();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region Other methods and handlers
        private bool Authorize()
        {
            card = new Card(txtCardNumber.Text, txtPIN.Text);
            //ATMRequestData data = new ATMRequestData(txtCardNumber.Text, txtPIN.Text, txtPhone.Text, (bool)chBoxIsCredit.IsChecked);
            //executing contract method and getting response:
            bool authorized = channel.Authorize(card);
            return authorized;
        }

        private void CreateChannel()
        {
            factory = new ChannelFactory<IATMContract>(binding, new EndpointAddress(address));
            channel = factory.CreateChannel();
        }

        private void ClearAllTextBoxes()
        {
            txtCardNumber.Text = txtPIN.Text = txtPhone.Text = txtPhoneSum.Text = string.Empty;
        }

        private void txtResponses_TextChanged(object sender, TextChangedEventArgs e)
        {
            txtResponses.ScrollToEnd();
        }
        #endregion
    }
}
