﻿using ATMLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ATMService
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Uri address = new Uri("http://localhost:4000/IContract");
        BasicHttpBinding binding = new BasicHttpBinding();
        ServiceHost service;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (service == null)
                {
                    service = new ServiceHost(typeof(Service));
                    service.AddServiceEndpoint(typeof(IATMContract), binding, address);
                    service.Open();
                    txtInput.Text += $"Сервер запущен.  {DateTime.Now}{Environment.NewLine}";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (service != null)
                {
                    service.Close();
                    service = null; //на поведение-алгоритм сборщика мусора не влияет.
                                    //Это для "рестарта"
                    txtInput.Text += $"Сервер остановлен.  {DateTime.Now}{Environment.NewLine}";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txtInput_TextChanged(object sender, TextChangedEventArgs e)
        {
            txtInput.ScrollToEnd();
        }
    }
}
