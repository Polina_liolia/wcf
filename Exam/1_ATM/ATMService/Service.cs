﻿using System;
using ATMLibrary;
using System.Linq;
using System.Windows;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text.RegularExpressions;

namespace ATMService
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)] //one instance for all requests
    public class Service : IATMContract
    {
        private List<BankClient> clients;   //stores clients' cards data, that can be authorized

        public Service()
        {
            //initializing cards data list:
            clients = new List<BankClient>()
            {
               new BankClient(new Card("123456", "111"), 8900, 15000),
               new BankClient(new Card("789012", "222"), (decimal)230.34, 2000),
               new BankClient(new Card("987654", "333"), 12, 0)
            };
        }

        public bool Authorize(Card card)
        {
            bool authorized = false;
            string outputText = string.Empty;
            BankClient client = clients.FirstOrDefault(c => new CardsComparer().Equals(c.ClientCard, card) == true);
            if (client != null)
            {
                authorized = true;
                outputText = $"Клиент авторизирован: { card.CardNumber }";
            }
            else
            {
                outputText = $"Авторизация невозможна: { card.CardNumber }";
            }
            UiConnection.UpdateText($"{outputText} {Environment.NewLine}");
            return authorized;
        }

        public decimal BalanceCheck(Card card)
        {
            decimal balance = 0;
            string outputText = string.Empty;
            if (Authorize(card))
            {
                BankClient client = clients.FirstOrDefault(c => new CardsComparer().Equals(c.ClientCard, card) == true);
                balance = client.Balance;
                outputText = $"Баланс клиента {card.CardNumber}: {balance}{Environment.NewLine}";
            }
            else
            {
                outputText = "Невозможно проверить баланс: ошибка авторизации";
            }
            UiConnection.UpdateText($"{outputText} {Environment.NewLine}");
            return balance;
        }

        public bool SendMoneyToPhone(ATMRequestData data)
        {
            bool moneySent = false;
            string outputText = string.Empty;
            if (Authorize(data.CardData))
            {
                //check phone input format:
                if (!CheckPhoneFormat(data.Phone))
                {
                    outputText = "Невозможно пополнить счет: неверный формат номера телефона (правильно: +380111111111)";
                }
                else
                {
                    BankClient client = clients.FirstOrDefault(c => new CardsComparer().Equals(c.ClientCard, data.CardData) == true);
                    if (data.Sum <= client.Balance)
                    {
                        moneySent = true;
                        client.Balance -= data.Sum;
                        outputText = $"Счет пополнен. Баланс: ${client.Balance}";
                    }
                    else if (data.CreditSourse == true && data.Sum <= (client.Balance + client.CreditLimit))
                    {
                        moneySent = true;
                        client.CreditLimit -= data.Sum - client.Balance;
                        client.Balance = 0;
                        outputText = $"Счет пополнен. Баланс: ${client.Balance}, кредитный лимит: {client.CreditLimit}";
                    }
                    else
                    {
                        outputText = "Невозможно пополнить счет: недостаточно средств";
                    }
                }
            }
            else
            {
                outputText = "Невозможно пополнить счет: ошибка авторизации";
            }
            UiConnection.UpdateText($"{outputText} {Environment.NewLine}");
            return moneySent;
        }

        private bool CheckPhoneFormat(string phone)
        {
            return Regex.IsMatch(phone, @"^\+380\d{9}$");
        }

        internal static class UiConnection
        {
            //getting current main window link:
            static readonly MainWindow window1 = Application.Current.Windows.Cast<Window>().
                FirstOrDefault(window => window is MainWindow) as MainWindow;

            internal static void UpdateText(string @string)
            {
                window1.txtInput.Text += Environment.NewLine + @string;
            }
        }
    }
}