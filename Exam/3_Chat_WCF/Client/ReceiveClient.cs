﻿using ChatLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace Client
{
    public delegate void ReceviedMessage(string sender, string message);
    public delegate void GotNames(object sender, List<string> names);

    public class ReceiveClient : ChatService.ISendChatServiceCallback
    {
        public event ReceviedMessage MessageReceived;
        public event GotNames NewNames;

        ChatService.SendChatServiceClient chatClient = null;

        public void Start(ReceiveClient rc, string name)
        {
            InstanceContext instance = new InstanceContext(rc);
            chatClient = new ChatService.SendChatServiceClient(instance);
            chatClient.Start(name);
        }

        public void SendMessage(string msg, string sender, string receiver)
        {
            chatClient.SendMessage(msg, sender, receiver);
        }

        public void Stop(string name)
        {
            chatClient.Stop(name);
        }

        void ChatService.ISendChatServiceCallback.RecieveMessage(string msg, string sender)
        {
            if (MessageReceived != null)
                MessageReceived(sender, msg);
        }

        public void SendNames(string[] names)
        {
            if (NewNames != null)
                NewNames(this, names.ToList());
        }
    }
}
