﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ReceiveClient receiveClient = null;
        string userName;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void btn_Login_Click(object sender, RoutedEventArgs e)
        {
            if (txt_UserName.Text.Equals(string.Empty))
            {
                MessageBox.Show("Login can not be empty!", "No login", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            userName = txt_UserName.Text;
            receiveClient = new ReceiveClient();
            receiveClient.Start(receiveClient, userName);
            receiveClient.NewNames += ReceiveClient_NewNames;
            receiveClient.MessageReceived += ReceiveClient_MessageReceived;

            txt_UserName.IsEnabled = false;
            btn_Login.IsEnabled = false;
            txt_Msg.IsEnabled = true;
            btn_Send.IsEnabled = true;
            btn_SendToAll.IsEnabled = true;
        }

        private void ReceiveClient_MessageReceived(string sender, string message)
        {
            txt_Messages.Text += $"{sender}>>{message}{Environment.NewLine}";
        }

        private void ReceiveClient_NewNames(object sender, List<string> names)
        {
            lb_Users.Items.Clear();
            foreach(string name in names)
            {
                if(!name.Equals(userName))
                    lb_Users.Items.Add(name);
            }
        }

        private void btn_Send_Click(object sender, RoutedEventArgs e)
        {
            if (lb_Users.Items.Count != 0)
            {
                if(lb_Users.SelectedIndex != -1)
                {
                    string messageFullText = $"{userName}>>{txt_Msg.Text}{Environment.NewLine}";
                    txt_Messages.Text += messageFullText;
                    string receiverName = lb_Users.SelectedValue.ToString();
                    receiveClient.SendMessage(txt_Msg.Text, userName, receiverName);
                    txt_Msg.Clear();
                }
                else
                {
                    MessageBox.Show("No message receiver was selected!", "Can't send message", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("No users in chat!", "Can't send message", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btn_SendToAll_Click(object sender, RoutedEventArgs e)
        {
            if (lb_Users.Items.Count != 0)
            {
                string messageFullText = $"{userName}>>{txt_Msg.Text}{Environment.NewLine}";
                txt_Messages.Text += messageFullText;
                foreach (string receiverName in lb_Users.Items)
                {
                    receiveClient.SendMessage(txt_Msg.Text, userName, receiverName);
                }
                txt_Msg.Clear();
            }
            else
            {
                MessageBox.Show("No users in chat!", "Can't send message", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void txt_Messages_TextChanged(object sender, TextChangedEventArgs e)
        {
            txt_Messages.ScrollToEnd();
        }       

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            receiveClient.Stop(userName);
        } 
    }
}
