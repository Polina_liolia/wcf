﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace ChatLibrary
{
    [ServiceContract(CallbackContract = typeof(IReceiveChatService))] //represents the required opposite contract in a two-way (or duplex) message exchange
    public interface ISendChatService
    {
        [OperationContract(IsOneWay = true)]    //IsOneWay - for void methods (return nothing)
        void SendMessage(string msg, string sender, string reciever);
        [OperationContract(IsOneWay = true)]
        void Start(string name);
        [OperationContract(IsOneWay = true)]
        void Stop(string name);
    }
}
