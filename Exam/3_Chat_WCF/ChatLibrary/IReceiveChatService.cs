﻿using System.Collections.Generic;
using System.ServiceModel;

namespace ChatLibrary
{
    public interface IReceiveChatService
    {
        [OperationContract(IsOneWay =true)]
        void RecieveMessage(string msg, string sender);
        [OperationContract(IsOneWay =true)]
        void SendNames(List<string> names);
    }
}