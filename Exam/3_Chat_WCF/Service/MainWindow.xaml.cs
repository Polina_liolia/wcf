﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Service
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ChatService chatServise;
        private ServiceHost serviceHost;
        public MainWindow()
        {
            InitializeComponent();
        }

        //ListOfNames event handler
        private void chatService_ListOfNames(List<string>names, object sender)
        {
            lb_Users.Items.Clear();
            foreach(string name in names)
            {
                lb_Users.Items.Add(name);
            }
        }

        private void btn_Start_Click(object sender, RoutedEventArgs e)
        {
            chatServise = new ChatService();
            //binding handler to ListOfNamesChanged event:
            ChatService.ListOfNamesChanged += chatService_ListOfNames;
            serviceHost = new ServiceHost(typeof(ChatService));
            serviceHost.Open();
            lb_Info.Items.Add($"Chat service started at {DateTime.Now}");
        }

        private void btn_Stop_Click(object sender, RoutedEventArgs e)
        {
            if (chatServise != null && serviceHost != null)
            {
                chatServise.Close();
                serviceHost.Close();
                chatServise = null;
                serviceHost = null;
                lb_Info.Items.Add($"Chat service stopped at {DateTime.Now}");
            }
        }
    }
}
