﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChatLibrary;
using System.ServiceModel;
using System.Diagnostics;

namespace Service
{
    public delegate void ListOfNames(List<string> names, object sender);

    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class ChatService : ISendChatService
    {
        private Dictionary<string, IReceiveChatService> names = new Dictionary<string, IReceiveChatService>();
        public static event ListOfNames ListOfNamesChanged;

        public void Close()
        {
            names.Clear();
        }

        #region ISendChatService
        public void Start(string name)
        {
           try
            {
                if(!names.ContainsKey(name))
                {
                    IReceiveChatService callback = OperationContext.Current.GetCallbackChannel<IReceiveChatService>();
                    AddUser(name, callback);
                    SendNamesToAll();
                }
                else
                {
                   
                }
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        public void Stop(string name)
        {
            if(names.ContainsKey(name))
            {
                names.Remove(name);
                SendNamesToAll();
            }
        }

        public void SendMessage(string msg, string sender, string reciever)
        {
           if(names.ContainsKey(reciever))
            {
                IReceiveChatService recieverCallback = names[reciever];
                recieverCallback.RecieveMessage(msg, sender);
            }
        }
        #endregion

        #region Helpers
        private void SendNamesToAll()
        {
            List<string> allNames = names.Keys.ToList(); 
            foreach(KeyValuePair<string, IReceiveChatService> name in names)
            {
                IReceiveChatService proxy = name.Value;
                proxy.SendNames(allNames);
            }
            if (ListOfNamesChanged != null)
                ListOfNamesChanged(allNames, this);
        }

        private void AddUser(string name, IReceiveChatService callback)
        {
            names.Add(name, callback);
            if (ListOfNamesChanged != null)
            {
                ListOfNamesChanged(names.Keys.ToList(), this);
            }
        }
        #endregion

    }
}
