﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace MyObjects
{
    #region Контракт на основании интерфейсов с пользовательскими классами
    [ServiceContract]
    public interface IMyPointContract
    {
        [OperationContract]
        Point Add(Point a, Point b);
    }
    //Namespace = "OtherNamespace" - задаём имя для предсказуемого
    //одинакового именования класса при маршалинге
    [DataContract(Namespace = "OtherNamespace")]
    public class Point
    {
        [DataMember]
        public double _x;
        [DataMember]
        public double _y;
        public Point(double x, double y)
        {
            _x = x;
            _y = y;
        }
    }
    public class MyPointService : IMyPointContract
    {
        public Point Add(Point a, Point b)
        {
            return new Point(a._x + a._y, b._x + b._y);
        }
    }
    #endregion
    #region Контракт на основании унаследованных интерфейсов
    [ServiceContract]
    public interface IContactManager
    {
        [OperationContract]
        void addCustomer(Customer customer);
        [OperationContract]
        void addContact(Contact contract);
        [OperationContract]
        Contact[] getContacts();
    }
    [DataContract(Name = "OtherNamespace")]
    [KnownType(typeof(Customer))] //говорим, что вместо родителя можно принимать его наследника Customer
    //теперь мы соответствуем принцыпу LSP (Liskov Subs. princ.)
    public class Contact
    {
        [DataMember]
        public string FirstName;
        [DataMember]
        public string LastName;

    }
    [DataContract(Namespace = "OtherNamespace")]
    public class Customer : Contact
    {
        [DataMember]
        public int OrderNumber;
    }
    public class ContractService : IContactManager
    {
        List<Customer> m_Customers = new List<Customer>();
        public void addCustomer(Customer customer)
        {
            m_Customers.Add(customer);
            //MessageBox.Show(OperationContext.Current.RequestContext.RequestMessage.ToString(), 
            //    "SERVER AddCustomer()" + " " + this.GetHashCode().ToString());   
        }
        public void addContact(Contact contact)
        {
            m_Customers.Add(contact as Customer);
            //MessageBox.Show(OperationContext.Current.RequestContext.RequestMessage.ToString(), 
            //    "SERVER AddContact()" + " " + this.GetHashCode().ToString());
        }
        public Contact[] getContacts()
        {
            //MessageBox.Show(OperationContext.Current.RequestContext.RequestMessage.ToString(), 
            //    "SERVER GetContacts()" + " " + this.GetHashCode().ToString());
            return m_Customers.ToArray();
        }
    }
    #endregion
}
