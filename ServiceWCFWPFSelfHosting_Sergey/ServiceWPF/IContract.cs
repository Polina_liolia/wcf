﻿using System;
using System.ServiceModel;
namespace ServiceWPF
{
    [ServiceContract]
    interface IContract
    {
        [OperationContract]
        string doAction(string input);
    }
    /// <summary>
    /// Перегрузка методов в интерфейсе
    /// из-за автомвтического маршалинга (сериализация перед передачей и десериализация)
    /// запрещена: просто указание атрибута [OperationContract] недостаточно
    /// IMyContract1 для случая, когда клиент использует точно такой же
    /// IMyContract2 для случая, когда клиент использует явно неперегруженную структуру
    /// </summary>
    [ServiceContract]
    interface IMyContract1
    {
        [OperationContract(Name = "AddInt")]
        int Add(int a, int b);
        [OperationContract(Name = "AddDouble")]
        double Add(double a, double b);      
    }
    public class MyService1 : IMyContract1
    {
        public int Add(int a, int b)
        {
            Console.WriteLine(OperationContext.Current.RequestContext.RequestMessage.ToString());
            return a + b;
        }

        public double Add(double a, double b)
        {
            Console.WriteLine(OperationContext.Current.RequestContext.RequestMessage.ToString());
            return a + b;
        }
    }
    [ServiceContract]
    interface IMyContract2 //использует явные имена
    {
        [OperationContract(Name = "AddInt")]
        int Add(int a, int b);
        [OperationContract(Name = "AddDouble")]
        double Add(double a, double b);
    }
    public class MyService2 : IMyContract2
    {
        public int Add(int a, int b)
        {
            Console.WriteLine(OperationContext.Current.RequestContext.RequestMessage.ToString());
            return a + b;
        }

        public double Add(double a, double b)
        {
            Console.WriteLine(OperationContext.Current.RequestContext.RequestMessage.ToString());
            return a + b;
        }
    }

}
