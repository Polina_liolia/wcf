﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ServiceModel;
using MyObjects;

namespace ServiceWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Uri _address = new Uri("http://localhost:4000/IContract");
        private BasicHttpBinding _binding = new BasicHttpBinding();
        private ServiceHost _service;
        private ServiceHost _pointService;
        private ServiceHost _customerService;
        public MainWindow()
        {
            InitializeComponent();
            ServerMyContractInit();
        }
        private void ServerMyContractInit()
        {
            ServiceHost host1 = new ServiceHost(
                typeof(MyService1),
                new Uri("http://localhost:4001"));
            host1.AddServiceEndpoint(typeof(IMyContract1),
                new BasicHttpBinding(), "");
            host1.Open();
            ServiceHost host2 = new ServiceHost(
               typeof(MyService2),
               new Uri("http://localhost:4002"));
            host2.AddServiceEndpoint(typeof(IMyContract2),
                new BasicHttpBinding(), "");
            host2.Open();
        }
        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if(_service == null)
                {
                    _service = new ServiceHost(typeof(Service));
                    _service.AddServiceEndpoint(typeof(IContract), _binding, _address);
                    _service.Open();
                    txtInput.Text += $"Сервер для Service запущен.   {DateTime.Now}" + Environment.NewLine;
                }
                if(_pointService == null)
                {
                    _pointService = new ServiceHost(typeof(MyPointService));
                    _pointService.AddServiceEndpoint(typeof(IMyPointContract), new BasicHttpBinding(), 
                        new Uri("http://localhost:4000/IMyPointContract"));
                    _pointService.Open();
                    txtInput.Text += $"Сервер для MyPointService запущен.   {DateTime.Now}" + Environment.NewLine;
                }
                if (_customerService == null)
                {
                    _customerService = new ServiceHost(typeof(ContractService));
                    _customerService.AddServiceEndpoint(typeof(IContactManager), new WSHttpBinding(),
                        new Uri("http://localhost:4000/IContractManager"));
                    _customerService.Open();
                    txtInput.Text += $"Сервер для ContractService запущен.   {DateTime.Now}" + Environment.NewLine;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_service != null)
                {
                    _service.Close();
                    _service = null;//на поведение-алгоритм сборщика мусора не влияет
                                    //нужно для "рестарта"
                    txtInput.Text += $"Сервер для Service остановлен.   {DateTime.Now}" + Environment.NewLine;
                }
                if (_pointService != null)
                {
                    _pointService.Close();
                    _pointService = null;
                    txtInput.Text += $"Сервер для MyPointService остановлен.   {DateTime.Now}" + Environment.NewLine;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
    public class Service : IContract
    {
        public string doAction(string input)
        {
            UiConnection.UpdateText("От клиента пришло: " + input
                + Environment.NewLine);
            return $"Вы (сервер) сказали: {input}";
        }
    }
    internal static class UiConnection
    {
        //Получение ссылки на текущее (главное) окно
        private static readonly MainWindow _window1 = 
            Application.Current.Windows.Cast<Window>()
            .FirstOrDefault(window => window is MainWindow) as MainWindow;
        //to google 'string @string'
        internal static void UpdateText(string @string)
        {
            _window1.txtInput.Text += Environment.NewLine + @string;
        }
    }
}
