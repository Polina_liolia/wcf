﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace ClientConsole
{
    [ServiceContract]
    interface IMyContract1 //совпадает 1 в 1 с серверным(общая dll)
    {
        [OperationContract(Name = "AddInt")]
        int Add(int a, int b);
        [OperationContract(Name = "AddDouble")]
        double Add(double a, double b);
    }
    [ServiceContract]
    interface IMyContract2 //использует явные имена
    {
        [OperationContract]
        int AddInt(int a, int b);
        [OperationContract]
        double AddDouble(double a, double b);
    }
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.Title = "Client MyContract";
                IMyContract1 proxy1 = ChannelFactory<IMyContract1>.CreateChannel(
                    new BasicHttpBinding(), new EndpointAddress("http://localhost:4001"));
                Console.WriteLine("Press any key...");
                Console.ReadKey();
                using (proxy1 as IDisposable)
                {
                    int sum = proxy1.Add(1, 2);
                    Console.WriteLine($"int sum = {sum}");
                    double dSum = proxy1.Add(1.5, 2.5);
                    Console.WriteLine($"double sum = {dSum}");
                }
                IMyContract2 proxy2 = ChannelFactory<IMyContract2>.CreateChannel(
                    new BasicHttpBinding(), new EndpointAddress("http://localhost:4002"));
                Console.WriteLine("Press any key...");
                Console.ReadKey();
                using (proxy2 as IDisposable)
                {
                    int sum = proxy2.AddInt(3, 4);
                    Console.WriteLine($"int sum = {sum}");
                    double dSum = proxy2.AddDouble(3.5, 4.5);
                    Console.WriteLine($"double sum = {dSum}");
                }
                Console.WriteLine("Press any key...");
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Press any key...");
                Console.ReadKey();
            }
        }
    }
}
