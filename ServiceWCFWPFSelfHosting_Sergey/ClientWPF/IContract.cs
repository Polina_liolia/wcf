﻿using System.ServiceModel;
namespace ClientWPF
{
    [ServiceContract]
    interface IContract
    {
        [OperationContract]
        string doAction(string input);
    }
}
