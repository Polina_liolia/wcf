﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ServiceModel;
using MyObjects;

namespace ClientWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Uri _address = new Uri("http://localhost:4000/IContract");
        private BasicHttpBinding _binding = new BasicHttpBinding();
        private ChannelFactory<IContract> _factory;
        private IContract _channel;

        private Uri _address2 = new Uri("http://localhost:4000/IContractManager");
        private WSHttpBinding _binding2 = new WSHttpBinding();
        private ChannelFactory<IContactManager> _factory2;
        private IContactManager _channel2;

        public MainWindow()
        {
            InitializeComponent();
        }
        private void btnSend_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if((_factory == null) && (_factory2 == null))
                {
                    //реализация паттерна проектирования "Фабрика" + "Фабричный метод"
                    //внутри WCF от Microsoft
                    _factory = new ChannelFactory<IContract>(_binding, new EndpointAddress(_address));
                    _channel = _factory.CreateChannel();
                    _factory2 = new ChannelFactory<IContactManager>(_binding2, new EndpointAddress(_address2));
                    _channel2 = _factory2.CreateChannel();
                }
                if ((_factory != null && _channel != null) && (_factory2 != null && _channel2 != null))
                {
                    Customer customer = new Customer();
                    customer.FirstName = "Ivan";
                    customer.LastName = "Petrov";
                    customer.OrderNumber = 123;
                    _channel2.addContact(customer as Contact);

                    Contact[] contacts = _channel2.getContacts();

                    for (int i = 0; i < contacts.Length; i++)
                    {
                        txtOutput.Text += "->" + contacts[i].FirstName
                            + " " + contacts[i].LastName + Environment.NewLine; ;
                    }

                    txtOutput.Text += $"От клиента {txtInput.Text}" + Environment.NewLine;
                    txtOutput.Text += $"От сервера {_channel.doAction(txtInput.Text)}" + Environment.NewLine;
                    txtInput.Text = string.Empty;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txtOutput_TextChanged(object sender, TextChangedEventArgs e)
        {
            txtOutput.ScrollToEnd();
        }        
    }
}
