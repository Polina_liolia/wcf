﻿using MyContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace MyServices
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class MyServices : IMyContract, IDisposable
    {
        public void Dispose()
        {
            Console.WriteLine($"Closed object: {this.GetHashCode()}");
        }

        public string MyMethod()
        {
            Console.WriteLine($"Current Object: {this.GetHashCode()}");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(OperationContext.Current.RequestContext.RequestMessage.ToString(), $" {this.GetHashCode().ToString()}");
            Console.ForegroundColor = ConsoleColor.White;
            return this.GetHashCode().ToString();
        }
    }
}
