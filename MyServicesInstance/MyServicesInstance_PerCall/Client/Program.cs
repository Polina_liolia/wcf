﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.ServiceModel;
using MyContracts;
using MyServices;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "CLIENT";
            
            //Локальная сеть. Сессия 10 минут
            NetTcpBinding binding = new NetTcpBinding(SecurityMode.None);
            binding.ReliableSession.Enabled = true;
            binding.ReliableSession.InactivityTimeout = TimeSpan.FromMinutes(10);

            IMyContract chanel = ChannelFactory<IMyContract>.CreateChannel(binding, new EndpointAddress("net.tcp://localhost:808/MyService"));

            try
            {
                for(int i = 0; i < 10; i++)
                {
                    string hash = chanel.MyMethod();
                    Console.WriteLine(hash);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.WriteLine("Press any key...");
            Console.ReadKey();

        }
    }
}
