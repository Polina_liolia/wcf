﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.ServiceModel;
using MyContracts;
using MyServices;

namespace Hosting
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "SERVER";
            ServiceHost host = new ServiceHost(typeof(MyServices.MyServices));
            //Локальная сеть. Сессия 10 минут
            NetTcpBinding binding = new NetTcpBinding(SecurityMode.None);
            binding.ReliableSession.Enabled = true;
            binding.ReliableSession.InactivityTimeout = TimeSpan.FromMinutes(10);

            host.AddServiceEndpoint(typeof(IMyContract), binding, "net.tcp://localhost:808/MyService");
            host.Open();

            Console.WriteLine("Press any key...");
            Console.ReadKey();

            host.Close();
        }
    }
}
