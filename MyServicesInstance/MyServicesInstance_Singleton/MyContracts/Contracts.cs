﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace MyContracts
{
    [ServiceContract]
    public interface IMyContract
    {
        [OperationContract]
        string MyMethod();
    }

}
