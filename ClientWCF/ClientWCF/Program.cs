﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace ClientWCF
{
    class Program
    {
        static void Main(string[] args)
        {
            ChannelFactory<IContract> factory = new ChannelFactory<IContract>(new BasicHttpBinding(), new EndpointAddress("http://localhost/myfiratsite/MyService.svc"));

            IContract channel = factory.CreateChannel();

            string response_str = channel.Method("apple");
            Console.WriteLine(response_str);

            Console.WriteLine("Для завершения нажмите <Any Key>.");
            Console.ReadKey();
        }

    }
}
