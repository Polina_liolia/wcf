﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace ClientWCF
{
    [ServiceContract]
    public interface IContract
    {
        [OperationContract]
        string Method(string s);
    }
}
