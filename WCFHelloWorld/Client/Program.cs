﻿using ContractLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace Client
{
    class Program
    {
        //Client - message sender
        static void Main(string[] args)
        {
            Console.Title = "CLIENT";
            // Указание, где ожидать входящие сообщения.
            Uri address = new Uri("http://localhost:4000/IContract");
            // Указание, как обмениваться сообщениями.
            BasicHttpBinding binding = new BasicHttpBinding();
            // Создание Конечной Точки.
            EndpointAddress endpoint = new EndpointAddress(address);
            // Создание фабрики каналов.
            ChannelFactory<IContract> factory = new ChannelFactory<IContract>(binding, endpoint);
            // Использование factory для создания канала (прокси) 
            //В WCF реализованы паттерны Proxi и Factory
            //При помощи Proxi 
            IContract channel = factory.CreateChannel();
            // Использование канала для отправки сообщения получателю и приема ответа.
            string response = channel.Say("Hello WCF!");
            Console.WriteLine(response);
            // Задержка.
            Console.ReadKey();
        }
    }
}
