﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;


namespace ContractLib
{
    [ServiceContract] //WCF attribute; data transfer class
    public interface IContract
    {
        [OperationContract] //WCF attribute; crossprocessor communication (several app domeins)
        string Say(string input);
    }
}
