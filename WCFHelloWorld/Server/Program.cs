﻿
using ContractLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "SERVER";
            //address, where to wait for incomming message:
            Uri address = new Uri("http://localhost:4000/IContract");  //ADDRESS (A)
            //creating binding, how to send and get messages:
            BasicHttpBinding binding = new BasicHttpBinding();          //BINDING (B)
            // BasicHttpSecurity - for secure connection
            //Contract - class, that implements a 'protocol' (set of rules):
            Type contract = typeof(IContract);                          //CONTRACT (C)
            //creating hosting provider for current service:
            ServiceHost host = new ServiceHost(typeof(Service));
            //adding the EndPoint
            host.AddServiceEndpoint(contract, binding, address);
            //starting waiting for incomming messages:
            host.Open();

            Console.WriteLine("App is ready to get messages");
            Console.ReadKey();
            //ending waiting for incomming messages:
            host.Close();
                
        }
    }
}
