﻿using ContractLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Server
{
    public class Service : IContract
    {
        //contract realization - IContract
        public string Say(string input)
        {
            //action on the server:
            Console.WriteLine($"Message recieved, body: {input}");
            //response to client:
            return "OK!";
        }
    }
}
