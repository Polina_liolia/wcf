﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace ServiceLibrary
{
    [ServiceContract]
    interface IContract
    {
        [OperationContract]
        string Method(string s);
    }

    public class MyService : IContract
    {
        public string Method(string s)
        {
            return s + " IIS";
        }
    }
}
