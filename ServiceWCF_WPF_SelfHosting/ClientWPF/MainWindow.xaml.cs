﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.ServiceModel;
using ContractLib;

namespace ClientWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Uri address = new Uri("http://localhost:4000/IContract");
        BasicHttpBinding binding = new BasicHttpBinding();
        ChannelFactory<IContract> factory;
        IContract channel;

        Uri cmAddress = new Uri("http://localhost:4000/IContractManager");
        WSHttpBinding cmBinding = new WSHttpBinding();
        ChannelFactory<IContractManager> cmFactory;
        IContractManager cmChannel;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnSendMsg_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (factory == null)
                {
                    //реализация паттерна проектирования "Фабрика" + "Фабричный метод" 
                    //внутри WCF от Microsoft
                    factory = new ChannelFactory<IContract>(binding, new EndpointAddress(address));
                    channel = factory.CreateChannel();
                }
                if (factory != null && channel != null)
                {
                    txtOutput.Text += $"От клиента  {txtInput.Text}{Environment.NewLine}";
                    txtOutput.Text += $"От сервера {channel.doAction(txtOutput.Text)} {Environment.NewLine}";
                    txtInput.Text += string.Empty;
                }

                if (cmFactory == null)
                {
                    //реализация паттерна проектирования "Фабрика" + "Фабричный метод" 
                    //внутри WCF от Microsoft
                    cmFactory = new ChannelFactory<IContractManager>(cmBinding, new EndpointAddress(cmAddress));
                    cmChannel = cmFactory.CreateChannel();
                }
                if (cmChannel != null && cmChannel != null)
                {
                    txtOutput.Text += $"От cm клиента  {txtInput.Text}{Environment.NewLine}";
                    Customer customer = new Customer() { FirstName = "Anton", LastName = "Pupkin", OrderNumber = 23 };
                    cmChannel.AddCustomer(customer);
                    txtOutput.Text += $"От cm сервера {cmChannel.GetContracts().Count()} {Environment.NewLine}";
                    txtInput.Text += string.Empty;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txtOutput_TextChanged(object sender, TextChangedEventArgs e)
        {
            txtOutput.ScrollToEnd();
        }
    }
}
