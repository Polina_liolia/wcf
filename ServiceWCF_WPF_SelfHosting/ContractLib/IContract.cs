﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.ServiceModel;

namespace ContractLib
{
    [ServiceContract]
    public interface IContract
    {
        [OperationContract]
        string doAction(string input);

        //маршаллинг - сериализация объекта перед его передачей и десериализация при его приёме
        //для объектов, связанных с автоматическим маршаллингом, существуют две проблемы, происходящие из платформозависимости:
        //- наследование (реализация унаследованного интефейса-контракта)
        //- перегрузка имен методов
    }


    //IMyContract1 - для случая, когда клиент использует точно такой же интерфейс
    [ServiceContract]
    public interface IMyContract1
    {
        [OperationContract(Name = "AddInt")] //явное переименование метода для маршаллинга
        int Add(int a, int b);

        [OperationContract(Name = "AddDouble")]
        double Add(double a, double b);
    }


    //IMyContract2 - для случая, когда клиент использует явно не перегруженную структуру
    [ServiceContract]
    public interface IMyContract2
    {
        [OperationContract(Name = "AddInt")] //явное переименование метода для маршаллинга
        int Add(int a, int b);

        [OperationContract(Name = "AddDouble")]
        double Add(double a, double b);
    }
   
}
