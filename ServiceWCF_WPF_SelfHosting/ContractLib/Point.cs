﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ContractLib
{
    //Контракт на основании интерфейсов с пользовательскими классами
    [ServiceContract]
    public interface IMyPointContract
    {
        [OperationContract]
        Point Add(Point a, Point b);
    }

    [DataContract (Namespace = "OtherNamespace")]
    public class Point
    {
        [DataMember]
        public double x;
        [DataMember]
        public double y;
        public Point(double x, double y)
        {
            this.x = x;
            this.y = y;
        }
    }

    public class MyPointService : IMyPointContract
    {
        public Point Add(Point a, Point b)
        {
            return new Point(a.y + a.x, b.y + b.x);
        }
    }
}
