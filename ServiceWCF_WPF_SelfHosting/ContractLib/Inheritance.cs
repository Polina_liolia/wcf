﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ContractLib
{
    //Контракт на основании унаследованных интерфейсов
    [ServiceContract]
    public interface IContractManager
    {
        [OperationContract]
        void AddCustomer(Customer customer);
        [OperationContract]
        void AddContract(Contract contract);
        [OperationContract]
        Contract[] GetContracts();
    }

    [DataContract(Name ="OtherNamespace")]
    [KnownType(typeof(Customer))] //атрибут для родителя - чтобы можно было использовать вместо него его наследника - соответствует принципу LSP
    public class Contract
    {
        public string FirstName;
        public string LastName;
    }

    public class Customer : Contract
    {
        [DataMember]
        public int OrderNumber;
    }
}
