﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace ClientMyContract
{
    [ServiceContract]
    public interface IMyContract1
    {
        [OperationContract(Name = "AddInt")] //явное переименование метода для маршаллинга
        int Add(int a, int b);

        [OperationContract(Name = "AddDouble")]
        double Add(double a, double b);
    }
}
