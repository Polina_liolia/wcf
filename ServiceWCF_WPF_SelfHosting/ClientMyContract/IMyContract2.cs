﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace ClientMyContract
{
    //IMyContract2 - для случая, когда клиент использует явно не перегруженную структуру
    [ServiceContract]
    public interface IMyContract2
    {
        [OperationContract]
        int AddInt(int a, int b);

        [OperationContract]
        double AddDouble(double a, double b);
    }
}
