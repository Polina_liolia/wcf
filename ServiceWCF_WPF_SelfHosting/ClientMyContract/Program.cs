﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace ClientMyContract
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Client MyContract";
            Console.WriteLine("Press any key to connect to servise...");
            Console.ReadKey();
            Console.WriteLine("Client is using interface with the same structure as servise does:");
            IMyContract1 proxy1 = ChannelFactory<IMyContract1>.CreateChannel(new BasicHttpBinding(), new EndpointAddress("http://localhost"));
            using (proxy1 as IDisposable)
            {
                int sum = proxy1.Add(2, 3);
                Console.WriteLine($"int sum = {sum}");
                double dSum = proxy1.Add(2.5, 3.5);
                Console.WriteLine($"double sum = {dSum}");
            }

            Console.WriteLine("Client is using interface with renamed methods:");
            IMyContract2 proxy2 = ChannelFactory<IMyContract2>.CreateChannel(new BasicHttpBinding(), new EndpointAddress("http://localhost:4002"));
            using (proxy1 as IDisposable)
            {
                int sum = proxy2.AddInt(2, 3);
                Console.WriteLine($"int sum = {sum}");
                double dSum = proxy2.AddDouble(2.5, 3.5);
                Console.WriteLine($"double sum = {dSum}");
            }

            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }
    }
}
