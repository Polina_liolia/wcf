﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.ServiceModel;
using ContractLib;

namespace ServiceWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Uri address = new Uri("http://localhost:4000/IContract");
        BasicHttpBinding binding = new BasicHttpBinding();
        ServiceHost service;
        ServiceHost pointService;
        ServiceHost customerService;
        public MainWindow()
        {
            InitializeComponent();
            ServerMyContractInit();
        }

        private void ServerMyContractInit()
        {
            ServiceHost host1 = new ServiceHost(typeof(MyService1), new Uri("http://localhost"));
            host1.AddServiceEndpoint(typeof(IMyContract1), new BasicHttpBinding(), "");
            host1.Open();

            ServiceHost host2 = new ServiceHost(typeof(MyService2), new Uri("http://localhost:4002"));
            host2.AddServiceEndpoint(typeof(IMyContract2), new BasicHttpBinding(), "");
            host2.Open();
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (service == null)
                {
                    service = new ServiceHost(typeof(Service));
                    service.AddServiceEndpoint(typeof(IContract), binding, address);
                    service.Open();
                    txtInput.Text += $"Сервер запущен.  {DateTime.Now}{Environment.NewLine}";
                }
                if(pointService == null)
                {
                    pointService = new ServiceHost(typeof(MyPointService));
                    pointService.AddServiceEndpoint(typeof(IMyPointContract), new BasicHttpBinding(), new Uri("http://localhost:4000/IMyPointContract"));
                    pointService.Open();
                    txtInput.Text += $"Point сервер запущен.  {DateTime.Now}{Environment.NewLine}";
                }
                if(customerService == null)
                {
                    customerService = new ServiceHost(typeof(ContractService));
                    customerService.AddServiceEndpoint(typeof(IContractManager), new WSHttpBinding(), new Uri("http://localhost:4000/IContractManager"));
                    customerService.Open();
                    txtInput.Text += $"Cusomer сервер заорапущен.  {DateTime.Now}{Environment.NewLine}";
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (service != null)
                {
                    service.Close();
                    service = null; //на поведение-алгоритм сборщика мусора не влияет.
                                    //Это для "рестарта"
                    txtInput.Text += $"Сервер остановлен.  {DateTime.Now}{Environment.NewLine}";
                }
                if(pointService != null)
                {
                    pointService.Close();
                    pointService = null;
                    txtInput.Text += $"Point сервер остановлен.  {DateTime.Now}{Environment.NewLine}";
                }
                if(customerService != null)
                {
                    customerService.Close();
                    customerService = null;
                    txtInput.Text += $"Customer сервер остановлен.  {DateTime.Now}{Environment.NewLine}";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
