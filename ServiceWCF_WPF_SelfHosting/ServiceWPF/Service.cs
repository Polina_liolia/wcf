﻿using ContractLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Windows;

namespace ServiceWPF
{
    public class Service : IContract
    {
        public string doAction(string input)
        {
            UiConnection.UpdateText($"От клиента пришло: {input} {Environment.NewLine}");
            return $"Вы (сервер) сказали: {input}";
        }
    }


    public class MyService1 : IMyContract1
    {
        public int Add(int a, int b)
        {
            Console.WriteLine(OperationContext.Current.RequestContext.RequestMessage.ToString());
            return a + b;
        }

        public double Add(double a, double b)
        {
            Console.WriteLine(OperationContext.Current.RequestContext.RequestMessage.ToString());
            return a + b;
        }
    }


    public class MyService2 : IMyContract2
    {
        public int Add(int a, int b)
        {
            Console.WriteLine(OperationContext.Current.RequestContext.RequestMessage.ToString());
            return a + b;
        }

        public double Add(double a, double b)
        {
            Console.WriteLine(OperationContext.Current.RequestContext.RequestMessage.ToString());
            return a + b;
        }
    }

    public class ContractService : IContractManager
    {
        List<Customer> customers = new List<Customer>();

        public void AddCustomer(Customer customer)
        {
            customers.Add(customer);
        }

        public void AddContract(Contract contract)
        {
            customers.Add(contract as Customer);
        }

        public Contract[] GetContracts()
        {
            return customers.ToArray();
        }
    }

    internal static class UiConnection
    {
        //получение ссылки на текущее главное окно
        static readonly MainWindow window1 = Application.Current.Windows.Cast<Window>().
            FirstOrDefault(window => window is MainWindow) as MainWindow;

        internal static void UpdateText(string @string)
        {
            window1.txtInput.Text += Environment.NewLine + @string;
        }
    }
}